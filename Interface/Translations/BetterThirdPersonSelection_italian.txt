$BetterThidPersonSelection	Better Third Person Selection



$BetterThirdPersonSelection_GeneralPage	General
$BetterThirdPersonSelection_GeneralSettings_HeaderText	General Settings

$BetterThirdPersonSelection_IsEnabledInFirstPerson_OptionText	Enabled in First Person
$BetterThirdPersonSelection_IsEnabledInFirstPerson_InfoText	enable/disable custom object selection algorithm in first person. Doesn't affect how the selected object is displayed.

$BetterThirdPersonSelection_IsEnabledInThirdPerson_OptionText	Enabled in Third Person
$BetterThirdPersonSelection_IsEnabledInThirdPerson_InfoText	enable/disable custom object selection algorithm in third person. Doesn't affect how the selected object is displayed.

$BetterThirdPersonSelection_IsEnabledOnHorseBack_OptionText	Enabled on Horseback
$BetterThirdPersonSelection_IsEnabledOnHorseBack_InfoText	enable/disable custom object selection algorithm while on horseback. Doesn't affect how the selected object is displayed.

$BetterThirdPersonSelection_IsEnabledInCombat_OptionText	Enabled in Combat
$BetterThirdPersonSelection_IsEnabledInCombat_InfoText	enable/disable custom object selection algorithm while in combat. Doesn't affect how the selected object is displayed.

$BetterThirdPersonSelection_IsEnabledWithWeaponsDrawn_OptionText	Enabled with Weapons Drawn
$BetterThirdPersonSelection_IsEnabledWithWeaponsDrawn_InfoText	enable/disable custom object selection algorithm while player has weapons drawn. Doesn't affect how the selected object is displayed.



$BetterThirdPersonSelection_ControlSettings_HeaderText	Control Settings

$BetterThirdPersonSelection_EnableObjectCycle_OptionText	Object Cycling Enabled
$BetterThirdPersonSelection_EnableObjectCycle_InfoText	enable/disable cycling between all valid objects in range with [modifier key] + [object cycling up/down key]

$BetterThirdPersonSelection_EnableContinuousObjectCycle_OptionText	Continuous Object Cycling Enabled
$BetterThirdPersonSelection_EnableContinuousObjectCycle_InfoText	if cycling reaches the maximum or minimum index, it will loop back so you can continue cycling.

$BetterThirdPersonSelection_ObjectCycleModifierKey_OptionText	Object Cycling Modifier Key
$BetterThirdPersonSelection_ObjectCycleModifierKey_InfoText	if enabled, use this + [object cycling up/down key] to cycle between all valid objects in range

$BetterThirdPersonSelection_ObjectCycleUpKey_OptionText	Object Cycling - Up Key
$BetterThirdPersonSelection_ObjectCycleUpKey_InfoText	if enabled, use [modifier key] + this to cycle between all valid objects in range. RECOMMENDED TO LEAVE ON MOUSEWHEEL.

$BetterThirdPersonSelection_ObjectCycleDownKey_OptionText	Object Cycling - Down Key
$BetterThirdPersonSelection_ObjectCycleDownKey_InfoText	if enabled, use [modifier key] + this to cycle between all valid objects in range. RECOMMENDED TO LEAVE ON MOUSEWHEEL.

$BetterThirdPersonSelection_CycleDelayMS_OptionText	Object Cycling - Key Delay
$BetterThirdPersonSelection_CycleDelayMS_InfoText	delay in milliseconds between registering cycle up/down key inputs.



$BetterThirdPersonSelection_SelectionRanges_HeaderText	Selection Range Settings

$BetterThirdPersonSelection_MaxAngleDif_OptionText	Max Angle
$BetterThirdPersonSelection_MaxAngleDif_InfoText	how closely the camera has to point at the object (2.0 = full 360 degrees around the player are accepted)

$BetterThirdPersonSelection_MaxAngleDif_HorseBack_OptionText	Max Angle (Horseback)
$BetterThirdPersonSelection_MaxAngleDif_HorseBack_InfoText	how closely the camera has to point at the object while on horseback (2.0 = full 360 degrees around the player are accepted)

$BetterThirdPersonSelection_MaxInteractionRange_OptionText	Interaction Range
$BetterThirdPersonSelection_MaxInteractionRange_InfoText	max allowed distance - in Skyrim units - between the character and the object. The native crosshair selection is exempt from this and still uses the value determined by the base game. HIGH VALUES DESTROY PERFORMANCE.

$BetterThirdPersonSelection_MaxInteractionRange_HorseBack_OptionText	Interaction Range (Horseback)
$BetterThirdPersonSelection_MaxInteractionRange_HorseBack_InfoText	max allowed distance - in Skyrim units - between the character and the object, while on horseback. The native crosshair selection is exempt from this and still uses the value determined by the base game. HIGH VALUES DESTROY PERFORMANCE.



$BetterThirdPersonSelection_HoldToDismount_HeaderText	Hold To Dismount Settings

$BetterThirdPersonSelection_IsHoldToDismountEnabled_HorseBack_OptionText	Enable Hold to Dismount
$BetterThirdPersonSelection_IsHoldToDismountEnabled_HorseBack_InfoText	While mounted, this makes it so that holding the activation key dismounts the character, instead of pressing the activation key once without anything selected.

$BetterThirdPersonSelection_HoldToDismountTime_HorseBack_OptionText	Time to Hold
$BetterThirdPersonSelection_HoldToDismountTime_HorseBack_InfoText	With Hold to Dismount enabled, this is the time in seconds the activation key has to be held down to dismount.

$BetterThirdPersonSelection_HoldToDismountMinTime_HorseBack_OptionText	Min Time to Hold
$BetterThirdPersonSelection_HoldToDismountMinTime_HorseBack_InfoText	With Hold to Dismount enabled, this is the time in seconds the activation key has to be held down until the progress circle appears.



$BetterThirdPersonSelection_WidgetPage	Widgets
$BetterThirdPersonSelection_WidgetSettings_HeaderText	Widget Settings

$BetterThirdPersonSelection_IsWidget3DEnabledInFirstPerson_OptionText	3D Widget Enabled in First Person
$BetterThirdPersonSelection_IsWidget3DEnabledInFirstPerson_InfoText	enable/disable 3D widget for the selected object in first person.

$BetterThirdPersonSelection_IsWidget3DEnabledInThirdPerson_OptionText	3D Widget Enabled in Third Person
$BetterThirdPersonSelection_IsWidget3DEnabledInThirdPerson_InfoText	enable/disable 3D widget for the selected object in third person.

$BetterThirdPersonSelection_IsWidget3DEnabledOnHorseBack_OptionText	3D Widget Enabled on Horseback
$BetterThirdPersonSelection_IsWidget3DEnabledOnHorseBack_InfoText	enable/disable 3D widget for the selected object while on horseback.

$BetterThirdPersonSelection_IsWidget3DEnabledInCombat_OptionText	3D Widget Enabled in Combat
$BetterThirdPersonSelection_IsWidget3DEnabledInCombat_InfoText	enable/disable 3D widget for the selected object while in combat.

$BetterThirdPersonSelection_IsWidget3DEnabledWithWeaponsDrawn_OptionText	3D Widget Enabled with Weapons Drawn
$BetterThirdPersonSelection_IsWidget3DEnabledWithWeaponsDrawn_InfoText	enable/disable 3D widget for the selected object while player has weapons drawn.



$BetterThirdPersonSelection_WidgetStyleSettings_HeaderText	3D Widget Style

$BetterThirdPersonSelection_IsIsDividerEnabled_OptionText	Enable Gray Bar Divider
$BetterThirdPersonSelection_IsDividerEnabled_InfoText	enable/disable gray bar divider between object name and weight/value.

$BetterThirdPersonSelection_IsActivationButtonEnabled_OptionText	Enable Activation Button
$BetterThirdPersonSelection_IsActivationButtonEnabled_InfoText	enable/disable activation button widget.

$BetterThirdPersonSelection_IsItemInfoEnabled_OptionText	Enable Item Info
$BetterThirdPersonSelection_IsItemInfoEnabled_InfoText	enable/disable item info details like weight/value.

$BetterThirdPersonSelection_IsDismountProgressCirlcleEnabled_OptionText	Enable Dismount Progress Circle
$BetterThirdPersonSelection_IsDismountProgressCirlcleEnabled_InfoText	enable/disable animated circle in the center of the screen when dismounting a horse.

$BetterThirdPersonSelection_WidgetAlpha_OptionText	Widget Opacity
$BetterThirdPersonSelection_WidgetAlpha_InfoText	Opacity/Alpha of all widgets (0.0-100.0). Also applies to 2D widget

$BetterThirdPersonSelection_WidgetZOffset_OptionText	3D Widget Height Offset
$BetterThirdPersonSelection_WidgetZOffset_InfoText	moves the 3D widget on some objects further up (or down for negative values). Doesn't apply to objects where the widget is placed in the center, or if the widget on the object was moved through a config file

$BetterThirdPersonSelection_WidgetZOffsetFirstPerson_OptionText	3D Widget Height Offset (First Person)
$BetterThirdPersonSelection_WidgetZOffsetFirstPerson_InfoText	moves the 3D widget on some objects further up (or down for negative values), this one only applies while in first person



$BetterThirdPersonSelection_2DWidgetStyleSettings_HeaderText	2D Widget Style

$BetterThirdPersonSelection_Widget2DSize_OptionText	2D Widget Size
$BetterThirdPersonSelection_Widget2DSize_InfoText	Size of the 2D widget. That is the widget that is used if the 3D widget is currently disabled.

$BetterThirdPersonSelection_Widget2DXPos_OptionText	2D Widget Pos X
$BetterThirdPersonSelection_Widget2DXPos_InfoText	X position of the 2D widget, as percentage of the screen width (0.0 - 1.0)

$BetterThirdPersonSelection_Widget2DYPos_OptionText	2D Widget Pos Y
$BetterThirdPersonSelection_Widget2DYPos_InfoText	Y position of the 2D widget, as percentage of the screen height (0.0 - 1.0)



$BetterThirdPersonSelection_WidgetSizeMin_OptionText	Min Widget Size
$BetterThirdPersonSelection_WidgetSizeMin_InfoText	Widget size at the furthest possible distance to the camera.

$BetterThirdPersonSelection_WidgetSizeMax_OptionText	Max Widget Size
$BetterThirdPersonSelection_WidgetSizeMax_InfoText	Widget size at the closest possible distance to the camera.

$BetterThirdPersonSelection_WidgetSizeMin_HorseBack_OptionText	Min Widget Size (Horseback)
$BetterThirdPersonSelection_WidgetSizeMin_HorseBack_InfoText	Widget size at the furthest possible distance to the camera - while on horseback.

$BetterThirdPersonSelection_WidgetSizeMax_HorseBack_OptionText	Max Widget Size (Horseback)
$BetterThirdPersonSelection_WidgetSizeMax_HorseBack_InfoText	Widget size at the closest possible distance to the camera - while on horseback.



$BetterThirdPersonSelection_PriorityPage	Priority
$BetterThirdPersonSelection_PriorityPage_HeaderText	Priority Settings

$BetterThirdPersonSelection_AngleMult_OptionText	Angle Priority Multiplier
$BetterThirdPersonSelection_AngleMult_InfoText	Higher values mean that the difference in angle between the camera forward vector and the vector toward the object, will have more of an impact when determining which object to select.

$BetterThirdPersonSelection_DistMult_OptionText	Distance Priority Multiplier
$BetterThirdPersonSelection_DistMult_InfoText	Higher values mean that the distance toward the object will have more of an impact when determining which object to select.

$BetterThirdPersonSelection_TypeMult_OptionText	Type Priority Multiplier
$BetterThirdPersonSelection_TypeMult_InfoText	Higher values mean that the type of object will have more of an impact when determining which object to select. Eg. misc objects are less important than weapons. Those priorities are currently hard coded.

$BetterThirdPersonSelection_SpecificMult_OptionText	Specific Objects Priority Multiplier
$BetterThirdPersonSelection_SpecificMult_InfoText	Higher values mean that a specific list of object base IDs (currently hard coded) will be prioritized more over other objects. Only gold coins, currently.

$BetterThirdPersonSelection_ValueMult_OptionText	Gold Value Priority Multiplier
$BetterThirdPersonSelection_ValueMult_InfoText	Higher values mean that an object's gold value will have more of an impact when determining which object to select

$BetterThirdPersonSelection_MaxValue_OptionText	Max Gold Value
$BetterThirdPersonSelection_MaxValue_InfoText	Max value for internal usage of ValueMult. Setting this to a very low value means that small differences in gold value have a bigger impact on priority.

$BetterThirdPersonSelection_NativeSelectionBonus_OptionText	Native Selection Bonus
$BetterThirdPersonSelection_NativeSelectionBonus_InfoText	Straight up bonus for the object determined by Skyrim's native crosshair selection. Setting this to a very high value will cause the native selection to always be selected, if present.

$BetterThirdPersonSelection_PreviousSelectionBonus_OptionText	Previous Selection Bonus
$BetterThirdPersonSelection_PreviousSelectionBonus_InfoText	Straight up bonus for the previously selected object, if still valid. Should be a low, non-zero value to make the selection a little "sticky" - this avoids selection flickering.



$BetterThirdPersonSelection_PerformancePage	Performance
$BetterThirdPersonSelection_PerformancePage_HeaderText	Performance Settings

$BetterThirdPersonSelection_NumTracesHorizontal_OptionText	Traces Horizontal
$BetterThirdPersonSelection_NumTracesHorizontal_InfoText	Max number of ray traces to perform per object horizontally. Total number of traces is this * traces vertical. More traces = more precise occlusion detection, but worse performance. And vice versa.

$BetterThirdPersonSelection_NumTracesVertical_OptionText	Traces Vertical
$BetterThirdPersonSelection_NumTracesVertical_InfoText	Max number of ray traces to perform per object vertically. Total number of traces is this * traces horizontal. More traces = more precise occlusion detection, but worse performance. And vice versa.



$BetterThirdPersonSelection_DebugPage	Debug
$BetterThirdPersonSelection_DebugPage_HeaderText	Debug Settings

$BetterThirdPersonSelection_Draw_RayTraces_OptionText	Debug Ray Traces
$BetterThirdPersonSelection_Draw_RayTraces_InfoText	enable/disable Scaleform debugging ray traces. Only works while custom object selection algorithm is enabled.

$BetterThirdPersonSelection_Draw_Bounds_Box_OptionText	Debug Bounds - Box
$BetterThirdPersonSelection_Draw_Bounds_Box_InfoText	enable/disable Scaleform debugging boxes for closeby objects. Only works while custom object selection algorithm is enabled.

$BetterThirdPersonSelection_Draw_Bounds_Sphere_OptionText	Debug Bounds - Sphere
$BetterThirdPersonSelection_Draw_Bounds_Sphere_InfoText	enable/disable Scaleform debugging sphere to show the world bounds of the selected object. Only works while custom object selection algorithm is enabled.

$BetterThirdPersonSelection_Draw_Bounds_MinMax_OptionText	Debug Bounds - Box min/max
$BetterThirdPersonSelection_Draw_Bounds_MinMax_InfoText	enable/disable Scaleform debugging draws to showcase the min/max world positions of the bounding boxes of closeby objects. Only works while custom object selection algorithm is enabled.

$BetterThirdPersonSelection_DebugDrawThickness_OptionText	Line Thickness
$BetterThirdPersonSelection_DebugDrawThickness_InfoText	thickness in px for the debug draws.

$BetterThirdPersonSelection_DebugDrawAlpha_OptionText	Line Alpha
$BetterThirdPersonSelection_DebugDrawAlpha_InfoText	Alpha value for the debug draws.



$BetterThirdPersonSelection_AdditionalSettings_HeaderText	Additional Settings

$BetterThirdPersonSelection_HotReloadConfig_OptionText	Reload Filters
$BetterThirdPersonSelection_HotReloadConfig_InfoText	enable/disable reloading ObjectFilterList.toml every time the BTPS MCM closes. Useful while configuring the filter list.

$BetterThirdPersonSelection_ShowFurnitureMarkers_OptionText	Show furniture markers
$BetterThirdPersonSelection_ShowFurnitureMarkers_InfoText	enable/disable showing various furniture markers for NPCs.



$BetterThirdPersonSelection_FilterPage	Filters
$BetterThirdPersonSelection_FilterSettings_HeaderText	Filter Settings

$BetterThirdPersonSelection_Filter_InfoText	enable/disable filters.

$BetterThirdPersonSelection_EnableFiltersForNativeSelection_OptionText	Enable Filters for Crosshair Selection
$BetterThirdPersonSelection_EnableFiltersForNativeSelection_InfoText	enable/disable filters while BTPS selection isn't active, such as when in first person with BTPS selection disabled in first person (default)



$BetterThirdPersonSelection_CompatibilityPage	Compatibility
$BetterThirdPersonSelection_CompatibilitySettings_HeaderText	Compatibility Settings

$BetterThirdPersonSelection_AdjustMoreHudWidgets_OptionText	enable/disable MoreHUD compatibility
$BetterThirdPersonSelection_AdjustMoreHudWidgets_InfoText	if enabled (with MoreHUD installed), some MoreHUD elements will be moved around with the 3D widget. This can cause issues with some aspect ratios and I'm frankly too fed up with Scaleform to fix it. Restart required to properly take effect.