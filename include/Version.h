#pragma once

namespace Version
{
	inline constexpr std::size_t MAJOR = 0;
	inline constexpr std::size_t MINOR = 5;
	inline constexpr std::size_t PATCH = 8;
	inline constexpr auto NAME = "0.5.9"sv;
	inline constexpr auto PROJECT = "BetterThirdPersonSelection"sv;
}
